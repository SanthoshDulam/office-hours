const mongoose = require('mongoose')

const OfficeHoursSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  day: {
    type: String,
    required: true
  },
  starttime: {
    type: String,
    required: true
  },
  endtime: {
    type:String,
    required: true
  }
})
module.exports = mongoose.model('OfficeHours', OfficeHoursSchema)
