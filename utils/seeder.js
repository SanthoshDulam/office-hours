// set up a temporary (in memory) database
const Datastore = require('nedb')
// const userController = require('../controllers/user')
const LOG = require('../utils/logger.js')
const users = require('../data/users.json')
const officehours = require('../data/officehours.json')
const message = require('../data/message.json')
const appointment = require('../data/appointment.json')

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = new Datastore()
  db.loadDatabase()

  // insert the sample data into our data store
  db.insert(officehours)
  db.insert(message)
  db.insert(users)
  db.insert(appointment)
  // initialize app.locals (these objects will be available to our controllers)
  app.locals.officehours = db.find(officehours)
  app.locals.messages = db.find(message)
  app.locals.users = db.find(users)
  app.locals.appointments = db.find(appointment)
  LOG.debug(`${officehours.length} officehours`)
  LOG.debug(`${users.length} users`)
  LOG.debug(`${message.length} messages`)
  LOG.debug(`${appointment.length} appointments`)
}
